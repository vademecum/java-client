# java-client

## Requirements

Building the API client library requires [Maven](https://maven.apache.org/) to be installed.

## Installation

To install the API client library to your local Maven repository, simply execute:

```shell
mvn install
```

To deploy it to a remote Maven repository instead, configure the settings of the repository and execute:

```shell
mvn deploy
```

Refer to the [official documentation](https://maven.apache.org/plugins/maven-deploy-plugin/usage.html) for more information.

### Maven users

Add this dependency to your project's POM:

```xml
<dependency>
    <groupId>com.vademecumas.client</groupId>
    <artifactId>java-client</artifactId>
    <version>0.1.0</version>
    <scope>compile</scope>
</dependency>
```

### Gradle users

Add this dependency to your project's build file:

```groovy
compile "com.vademecumas.client:java-client:0.1.0"
```

### Others

At first generate the JAR by executing:

    mvn package

Then manually install the following JARs:

* target/java-client-0.1.0.jar
* target/lib/*.jar

## Getting Started

Please follow the [installation](#installation) instruction and execute the following Java code:

```java

import Vademecumas.Client.*;
import Vademecumas.Client.auth.*;
import Vademecumas.Client.Model.*;
import Vademecumas.Client.Api.AuthenticationApi;

import java.io.File;
import java.util.*;

public class AuthenticationApiExample {

    public static void main(String[] args) {
        ApiClient defaultClient = Configuration.getDefaultApiClient();
        
        // Configure HTTP basic authorization: basicAuth
        HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
        basicAuth.setUsername("YOUR USERNAME");
        basicAuth.setPassword("YOUR PASSWORD");

        AuthenticationApi apiInstance = new AuthenticationApi();
        try {
            AuthResponse result = apiInstance.auth();
            System.out.println(result);
        } catch (ApiException e) {
            System.err.println("Exception when calling AuthenticationApi#auth");
            e.printStackTrace();
        }
    }
}

```

## Documentation for API Endpoints

All URIs are relative to *https://api-test.vademecumonline.com.tr/v1*

Class | Method | HTTP request | Description
------------ | ------------- | ------------- | -------------
*AuthenticationApi* | [**auth**](docs/AuthenticationApi.md#auth) | **GET** /auth-token | Returns authentication tokens to be used.
*ProductRequestsApi* | [**getProductAllergies**](docs/ProductRequestsApi.md#getProductAllergies) | **GET** /product-allergy/{productId} | Returns product allergies
*ProductRequestsApi* | [**getProductCard**](docs/ProductRequestsApi.md#getProductCard) | **GET** /product-card/{productId} | Returns a product card
*ProductRequestsApi* | [**getProductCautionCard**](docs/ProductRequestsApi.md#getProductCautionCard) | **GET** /product-caution-card/{productId} | Returns a product caution card


## Documentation for Models

 - [AllergicReaction](docs/AllergicReaction.md)
 - [Allergy](docs/Allergy.md)
 - [AllergyInfo](docs/AllergyInfo.md)
 - [Atc](docs/Atc.md)
 - [AtcIndex](docs/AtcIndex.md)
 - [AuthResponse](docs/AuthResponse.md)
 - [AuthResponseData](docs/AuthResponseData.md)
 - [Company](docs/Company.md)
 - [ErrorDetails](docs/ErrorDetails.md)
 - [ErrorResponse](docs/ErrorResponse.md)
 - [NfcCode](docs/NfcCode.md)
 - [PrescriptionType](docs/PrescriptionType.md)
 - [Product](docs/Product.md)
 - [ProductAllergy](docs/ProductAllergy.md)
 - [ProductCard](docs/ProductCard.md)
 - [ProductCardCard](docs/ProductCardCard.md)
 - [ProductCardResponse](docs/ProductCardResponse.md)
 - [ProductCardSpecialInformation](docs/ProductCardSpecialInformation.md)
 - [Status](docs/Status.md)
 - [Substance](docs/Substance.md)


## Documentation for Authorization

Authentication schemes defined for the API:
### apiKey

- **Type**: API key
- **API key parameter name**: authorization
- **Location**: HTTP header

### basicAuth

- **Type**: HTTP basic authentication


## Recommendation

It's recommended to create an instance of `ApiClient` per thread in a multithreaded environment to avoid any potential issues.

## Author

birce@vademecumonline.com.tr

