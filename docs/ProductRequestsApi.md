# ProductRequestsApi

All URIs are relative to *https://api-test.vademecumonline.com.tr/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**getProductAllergies**](ProductRequestsApi.md#getProductAllergies) | **GET** /product-allergy/{productId} | Returns product allergies
[**getProductCard**](ProductRequestsApi.md#getProductCard) | **GET** /product-card/{productId} | Returns a product card
[**getProductCautionCard**](ProductRequestsApi.md#getProductCautionCard) | **GET** /product-caution-card/{productId} | Returns a product caution card


<a name="getProductAllergies"></a>
# **getProductAllergies**
> ProductAllergy getProductAllergies(productId)

Returns product allergies



### Example
```java
// Import classes:
//import Vademecumas.Client.ApiClient;
//import Vademecumas.Client.ApiException;
//import Vademecumas.Client.Configuration;
//import Vademecumas.Client.auth.*;
//import Vademecumas.Client.Api.ProductRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiKey
ApiKeyAuth apiKey = (ApiKeyAuth) defaultClient.getAuthentication("apiKey");
apiKey.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.setApiKeyPrefix("Token");

ProductRequestsApi apiInstance = new ProductRequestsApi();
Long productId = 789L; // Long | ID of product
try {
    ProductAllergy result = apiInstance.getProductAllergies(productId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductRequestsApi#getProductAllergies");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **Long**| ID of product |

### Return type

[**ProductAllergy**](ProductAllergy.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getProductCard"></a>
# **getProductCard**
> ProductCardResponse getProductCard(productId)

Returns a product card



### Example
```java
// Import classes:
//import Vademecumas.Client.ApiClient;
//import Vademecumas.Client.ApiException;
//import Vademecumas.Client.Configuration;
//import Vademecumas.Client.auth.*;
//import Vademecumas.Client.Api.ProductRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiKey
ApiKeyAuth apiKey = (ApiKeyAuth) defaultClient.getAuthentication("apiKey");
apiKey.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.setApiKeyPrefix("Token");

ProductRequestsApi apiInstance = new ProductRequestsApi();
Long productId = 789L; // Long | ID of product
try {
    ProductCardResponse result = apiInstance.getProductCard(productId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductRequestsApi#getProductCard");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **Long**| ID of product |

### Return type

[**ProductCardResponse**](ProductCardResponse.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

<a name="getProductCautionCard"></a>
# **getProductCautionCard**
> ProductCardResponse getProductCautionCard(productId)

Returns a product caution card



### Example
```java
// Import classes:
//import Vademecumas.Client.ApiClient;
//import Vademecumas.Client.ApiException;
//import Vademecumas.Client.Configuration;
//import Vademecumas.Client.auth.*;
//import Vademecumas.Client.Api.ProductRequestsApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure API key authorization: apiKey
ApiKeyAuth apiKey = (ApiKeyAuth) defaultClient.getAuthentication("apiKey");
apiKey.setApiKey("YOUR API KEY");
// Uncomment the following line to set a prefix for the API key, e.g. "Token" (defaults to null)
//apiKey.setApiKeyPrefix("Token");

ProductRequestsApi apiInstance = new ProductRequestsApi();
Long productId = 789L; // Long | ID of product
try {
    ProductCardResponse result = apiInstance.getProductCautionCard(productId);
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling ProductRequestsApi#getProductCautionCard");
    e.printStackTrace();
}
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **productId** | **Long**| ID of product |

### Return type

[**ProductCardResponse**](ProductCardResponse.md)

### Authorization

[apiKey](../README.md#apiKey)

### HTTP request headers

 - **Content-Type**: application/json
 - **Accept**: application/json

