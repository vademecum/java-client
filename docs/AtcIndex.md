
# AtcIndex

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**code** | **String** |  |  [optional]
**description** | **String** |  |  [optional]
**tree** | [**List&lt;Atc&gt;**](Atc.md) |  |  [optional]



