
# ProductCardSpecialInformation

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pediatricStatus** | [**Status**](Status.md) |  |  [optional]
**emergencyPediatricStatus** | [**Status**](Status.md) |  |  [optional]
**lightProtectionStatus** | [**Status**](Status.md) |  |  [optional]
**moistureProtectionStatus** | [**Status**](Status.md) |  |  [optional]
**coldChainStatus** | [**Status**](Status.md) |  |  [optional]
**cytotoxicStatus** | [**Status**](Status.md) |  |  [optional]
**additionalMonitoringStatus** | [**Status**](Status.md) |  |  [optional]
**orderedDistributionStatus** | [**Status**](Status.md) |  |  [optional]
**mandatoryChemoDrugStatus** | [**Status**](Status.md) |  |  [optional]
**dailyTreatmentStatus** | [**Status**](Status.md) |  |  [optional]
**indicationCompatibilityStatus** | [**Status**](Status.md) |  |  [optional]
**offLabelUsageStatus** | [**Status**](Status.md) |  |  [optional]
**hospitalProductStatus** | [**Status**](Status.md) |  |  [optional]
**dualPricedProductStatus** | [**Status**](Status.md) |  |  [optional]
**currentExchangeRateProductStatus** | [**Status**](Status.md) |  |  [optional]
**orphanDrugStatus** | [**Status**](Status.md) |  |  [optional]
**patientApprovalFormStatus** | [**Status**](Status.md) |  |  [optional]
**drugSafetyMonitoringFormStatus** | [**Status**](Status.md) |  |  [optional]
**ceCertificationStatus** | [**Status**](Status.md) |  |  [optional]
**highRiskStatus** | [**Status**](Status.md) |  |  [optional]
**freeMarketPriceStatus** | [**Status**](Status.md) |  |  [optional]



