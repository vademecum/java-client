# AuthenticationApi

All URIs are relative to *https://api-test.vademecumonline.com.tr/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**auth**](AuthenticationApi.md#auth) | **GET** /auth-token | Returns authentication tokens to be used.


<a name="auth"></a>
# **auth**
> AuthResponse auth()

Returns authentication tokens to be used.

Makes a token request to the API and returns the tokens that can used in the other requests. To use the token field in the other requests, append a &#39;__:__&#39; to the provided token value, encode it with base64 and set the API token as __&#39;*Basic base64ed_token_string_with_semicolon*__&#39;

### Example
```java
// Import classes:
//import Vademecumas.Client.ApiClient;
//import Vademecumas.Client.ApiException;
//import Vademecumas.Client.Configuration;
//import Vademecumas.Client.auth.*;
//import Vademecumas.Client.Api.AuthenticationApi;

ApiClient defaultClient = Configuration.getDefaultApiClient();

// Configure HTTP basic authorization: basicAuth
HttpBasicAuth basicAuth = (HttpBasicAuth) defaultClient.getAuthentication("basicAuth");
basicAuth.setUsername("YOUR USERNAME");
basicAuth.setPassword("YOUR PASSWORD");

AuthenticationApi apiInstance = new AuthenticationApi();
try {
    AuthResponse result = apiInstance.auth();
    System.out.println(result);
} catch (ApiException e) {
    System.err.println("Exception when calling AuthenticationApi#auth");
    e.printStackTrace();
}
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**AuthResponse**](AuthResponse.md)

### Authorization

[basicAuth](../README.md#basicAuth)

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

