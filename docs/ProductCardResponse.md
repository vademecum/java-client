
# ProductCardResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **Boolean** |  |  [optional]
**data** | [**ProductCard**](ProductCard.md) |  |  [optional]
**error** | [**ErrorDetails**](ErrorDetails.md) |  |  [optional]



