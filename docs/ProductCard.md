
# ProductCard

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**Product**](Product.md) |  |  [optional]
**card** | [**ProductCardCard**](ProductCardCard.md) |  |  [optional]



