
# Status

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**status** | **Boolean** |  |  [optional]
**description** | **String** |  |  [optional]



