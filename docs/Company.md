
# Company

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Long** |  |  [optional]
**name** | **String** |  |  [optional]
**officialName** | **String** |  |  [optional]
**website** | **String** |  |  [optional]
**fax** | **String** |  |  [optional]
**phone** | **String** |  |  [optional]
**address** | **String** |  |  [optional]
**email** | **String** |  |  [optional]
**city** | **String** |  |  [optional]
**district** | **String** |  |  [optional]



