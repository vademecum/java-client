
# ErrorResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **Boolean** |  |  [optional]
**error** | [**ErrorDetails**](ErrorDetails.md) |  |  [optional]



