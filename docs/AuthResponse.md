
# AuthResponse

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**success** | **Boolean** |  |  [optional]
**data** | [**AuthResponseData**](AuthResponseData.md) |  |  [optional]



