
# Substance

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | **Integer** |  |  [optional]
**name** | **String** |  |  [optional]
**alternativeNames** | **List&lt;String&gt;** |  |  [optional]



