
# AllergyInfo

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**info** | **String** |  |  [optional]
**desensitization** | **String** |  |  [optional]



