
# ProductCardCard

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**nfcCode** | [**NfcCode**](NfcCode.md) |  |  [optional]
**prescriptionType** | [**PrescriptionType**](PrescriptionType.md) |  |  [optional]
**equivalentGroups** | **List&lt;String&gt;** |  |  [optional]
**reimbursementStatus** | [**Status**](Status.md) |  |  [optional]
**specialInformation** | [**ProductCardSpecialInformation**](ProductCardSpecialInformation.md) |  |  [optional]
**sgkEquivalentCodes** | **List&lt;String&gt;** |  |  [optional]
**sgkPriceReferenceCodes** | **List&lt;String&gt;** |  |  [optional]
**company** | [**Company**](Company.md) |  |  [optional]
**publicNumber** | **String** |  |  [optional]
**atcIndices** | [**List&lt;AtcIndex&gt;**](AtcIndex.md) |  |  [optional]
**images** | [**List&lt;java.io.File&gt;**](java.io.File.md) |  |  [optional]
**shelfLife** | **String** |  |  [optional]
**notice** | **String** |  |  [optional]



