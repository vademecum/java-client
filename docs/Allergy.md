
# Allergy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**substance** | [**Substance**](Substance.md) |  |  [optional]
**substanceAllergyInfo** | [**AllergyInfo**](AllergyInfo.md) |  |  [optional]
**allergicReactions** | [**List&lt;AllergicReaction&gt;**](AllergicReaction.md) |  |  [optional]



