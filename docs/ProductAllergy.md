
# ProductAllergy

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**product** | [**Product**](Product.md) |  |  [optional]
**allergies** | [**List&lt;Allergy&gt;**](Allergy.md) |  |  [optional]



