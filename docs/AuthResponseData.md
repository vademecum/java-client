
# AuthResponseData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**token** | **String** |  |  [optional]
**iframeToken** | **String** |  |  [optional]
**expiresAt** | **Integer** |  |  [optional]



